var map = L.map('map', {
		minZoom: 7,
		maxZoom: 13});

// Steve Hansell https://stackoverflow.com/a/3291856
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

// "2014\/02\/27 00:00:00"
function niceDate(value) {
	var date = value.split(' ')[0].split('\/')
	var yr = parseInt(date[0])
	var mnt = parseInt(date[1])
	var day = parseInt(date[2])

	return day + '. ' + mnt + '. ' + yr
};

var datelIco = L.icon({
    iconUrl: 'datel_ico.png',

    iconSize:     [20, 47], // size of the icon
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    popupAnchor:  [-20, -78] // point from which the popup should open relative to the iconAnchor
});

var tetrevIco = L.icon({
    iconUrl: 'tetrev_ico.png',

    iconSize:     [20, 20], // size of the icon
    iconAnchor:   [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor:  [-20, -17] // point from which the popup should open relative to the iconAnchor
});

map.scrollWheelZoom.disable();

var background = L.tileLayer('https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png', {
	opacity: 1,
    attribution: 'mapová data © přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>,' 
    + ' zdroj dat <a target="_blank" href="http://www.birds.cz/avif/">Faunistická databáze ČSO</a>, ikony ptáků (CC) <a target="_blank" href="https://thenounproject.com/term/woodpecker/755059/">Dolly Holmes</a> & <a target="_blank" href="https://thenounproject.com/search/?q=capercaillie&i=774637">Juraj Sedlák</a>'
});

var labels = L.tileLayer('https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png', {
	opacity: 1
});

var datel = L.markerClusterGroup();

$.getJSON( "./datlik_tetrev_sumava.json", function(data) {
  for (feature in data['features']) {
  	var ftr = data['features'][feature]
  	var mkr;

  	if (ftr.properties.TaxonNameCZ == 'datlík tříprstý') {
  		mkr = datelIco;
  	} else {
  		mkr = tetrevIco;
  	};

  	var marker = L.marker(new L.LatLng(ftr.geometry.coordinates[1], ftr.geometry.coordinates[0]), {
  		'icon': mkr,
  	});
  	marker.bindPopup(ftr.properties.TaxonNameCZ.capitalize() + ' byl poblíž obce <b>' 
  		+ ftr.properties.NearestMun + '</b> pozorován ' + niceDate(ftr.properties.Date_) + '<br>'
  		+ 'Detaily <a target="_blank" href="' + ftr.properties.Link + '">v databázi AVIF</a>.', {
  			maxWidth : 560
  		});
  	datel.addLayer(marker)
  }
});

map.setView([49.0344489, 13.5260611], 10)
			.addLayer(background)
			.addLayer(datel)
			.addLayer(labels)			